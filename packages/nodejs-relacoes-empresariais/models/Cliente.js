const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },
    timestamps: {}
};

const ClienteSchema = new mongoose.Schema({
    codigoExterno: {
        type: String,
        required: true,
        index: true
    },
    loja: {
        type: String,
        required: true,
        index: true
    },
    nome: {
        type: String,
        required: true
    },
    ativo: {
        type: Boolean,
        default: true
    },
    endereco: {
        type: String,
        required: true
    },
    cep: {
        type: String,
        required: true
    },
    municipio: {
        type: String,
        required: true
    },
    bairro: {
        type: String,
        required: true
    },
    cpfcnpj: {
        type: String,
        required: true
    },
    dataNascimento: {
        type: Date,
    },
    uf: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'UnidadeFederativa'
    },
    usuarios: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Usuario'
        }
    ]
}, opts)

ClienteSchema.index(
    {
        codigoExterno: 'text',
        nome: 'text',
        cpf: 'text'
    },
)
ClienteSchema.index(
    {
        codigoExterno: 1,
        loja: 1
    },
    {
        unique: true
    }

);


module.exports = mongoose.model("Cliente", ClienteSchema);