const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const FilialSchema = new mongoose.Schema({
    
    ativo: {
        type: Boolean,
        default: true,
    },
    codigoExterno: {
        type: String,
        required: true,
    },
    razaoSocial: {
        type: String,
        required: true,
    },
    nomeFantasia: {
        type: String,
        required: true,
    },
    cnpj: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    },
    dataAbertura: {
        type: Date,
        required: true,
    },
    inscricaoEstadual: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    },
    inscricaoMunicipal: {
        type: String,
    },
    endereco: {
        type: String,
        required: true,
    },
    bairro: {
        type: String,
        required: true,
    },
    cidade: {
        type: String,
        required: true,
    },
    unidadeFederativa:{
        type: String,
        required: true,
    },
    cep: {
        type: String,
        required: true,
    },
    localDevolucaoEmbalagens: {
        type: String
    },
}, opts)

FilialSchema.index({
    codigoExterno: 'text',
    cnpj: 'text',
    nomeFantasia: 'text'
});

module.exports = mongoose.model("Filial", FilialSchema);