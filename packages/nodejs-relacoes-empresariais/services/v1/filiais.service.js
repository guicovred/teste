const { MoleculerError } = require("moleculer").Errors

const Filial = require('../../models/Filial')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(Filial)],
    name: "filiais",
    version: 1,

    settings: {
        fields: ["_id", "ativo", "codigoExterno", "razaoSocial", "nomeFantasia", "cnpj", "dataAbertura", "inscricaoEstadual", "inscricaoMunicipal", "endereco", "bairro", "cidade", "unidadeFederativa", "cep", "localDevolucaoEmbalagens"],
        populates: {
            "unidadeFederativa": "v1.unidades-federativas.get"
        }
    },
   
}



