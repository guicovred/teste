const Cliente = require("../../models/Cliente")
const DbService = require("../../mixins/db.mixin")

module.exports = {
    mixins: [DbService(Cliente)],
    name: "clientes",
    version: 1,
    settings: {
        fields: ["_id", "codigoExterno", "nome", "ativo", "endereco", "cep", "municipio", "bairro", "cpfcnpj", "uf", "usuarios", "dataNascimento", 'loja'],
        populates: {
            "uf": "v1.unidades-federativas.get"
        }
        
    }
}