import {FakeUsuariosRepository} from '../repositories/fakes/FakeUsuariosRepository';
import {CreateUsuarioService} from './CreateUsuarioService';

describe('CreateUsuarioService', () => {
  it('should be able to create a user', async () => {
    const repository = new FakeUsuariosRepository();
    const service = new CreateUsuarioService(repository);

    const usuario = await service.execute({
      nome: 'John Doe',
      cpf: '12312312387',
      email: 'johndoe@foobar.com',
    });

    expect(usuario).toBeDefined();
    expect(usuario).toHaveProperty('id');
    expect(usuario.cpf).toBe('12312312387');
    expect(usuario.email).toBe('johndoe@foobar.com');
  });
});
