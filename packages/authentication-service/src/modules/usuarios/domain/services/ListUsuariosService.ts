import {Usuario} from '../model/Usuario';
import {IUsuariosRepository} from '../repositories/IUsuariosRepository';

type SortType = 'asc' | 'desc'

export interface IListUsuariosServiceRequest {
  page: number;
  pageSize: number;
  search?: string;
  sorting?: {
    [key: string]: SortType;
  };
}

export interface IListUsuariosServiceResponse {
  rows: Usuario[];
  page: number;
  total: number;
  pageSize: number;
  totalPages: number;
}

/**
    * Serviço de listagem de usuários.
    */
export class ListUsuariosService {
  private usuariosRepository: IUsuariosRepository;

  /**
   * Construtor para a listagem de usuários
   * @param {object} usuariosRepository - Dados do repositório de usuários
   */
  constructor(usuariosRepository: IUsuariosRepository) {
    this.usuariosRepository = usuariosRepository;
  }

  /**
    * Efetua a listagem de usuários.
    * @param {object} data - Dados da tabela de usuários.
    */
  async execute(data?: IListUsuariosServiceRequest): Promise<IListUsuariosServiceResponse> {
    const searchFields = ['nome'];

    data.page = data.page || 1;
    data.pageSize = data.pageSize || 15;

    const rows = await this.usuariosRepository.list({
      paging: {
        page: data.page,
        size: data.pageSize,
      },
      search: data?.search && {
        fields: searchFields,
        type: 'contains',
        value: data.search,
      },
      sorting: data?.sorting,
    });

    const total = await this.usuariosRepository.count({
      search: data?.search && {
        fields: searchFields,
        type: 'contains',
        value: data.search,
      },
    });

    return {
      rows,
      total,
      page: data.page,
      pageSize: data.pageSize,
      totalPages: Math.ceil(total / data.pageSize),
    };
  }
}
