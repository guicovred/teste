import {IUsuariosRepository} from '../repositories/IUsuariosRepository';

export interface ICreateUsuarioServiceRequest {
  nome: string;
  cpf: string;
  email?: string;
  telefone?: string;
  papeis?: string[];
}
export interface ICreateUsuarioServiceResponse {
  id: string;
  nome: string;
  cpf: string;
  email?: string;
  telefone?: string;
  papeis?: string[];
}

/**
  * Serviço de criação de usuários.
  */
export class CreateUsuarioService {
  private usuariosRepository: IUsuariosRepository;

  /**
   * Construtor para acriação de usuário
   * @param {object} usuariosRepository - Repositório para a criação de usuários
   */
  constructor(usuariosRepository: IUsuariosRepository) {
    this.usuariosRepository = usuariosRepository;
  }

  /**
    * Efetua a criação de um usuário
    * @param {object} data - Dados do usuário.
    * @return {object} - Retorna o usuário criado
    */
  async execute(data: ICreateUsuarioServiceRequest): Promise<ICreateUsuarioServiceResponse> {
    const usuario = await this.usuariosRepository.create(data);

    return {
      id: usuario.id,
      nome: usuario.nome,
      cpf: usuario.cpf,
      email: usuario.email,
      telefone: usuario.telefone,
      papeis: usuario.papeis,
    };
  }
}
