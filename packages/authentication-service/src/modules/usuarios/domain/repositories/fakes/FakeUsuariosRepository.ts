import {CountUsuariosDTO} from '../../../dtos/CountUsuariosDTO';
import {ListUsuariosDTO} from '../../../dtos/ListUsuariosDTO';
import {Usuario} from '../../model/Usuario';
import {IUsuariosRepository} from '../IUsuariosRepository';

/**
 * Repositório falso de usuários utilizado para testes unitários
 */
export class FakeUsuariosRepository implements IUsuariosRepository {
  private usuarios: Usuario[] = [];

  /**
   * Cria um usuário
   * @param {object} data - Dados do usuário a ser criado.
   * @return {object} - Retorna o usuário criado.
   */
  async create(data: Omit<Usuario, 'id'>): Promise<Usuario> {
    const usuario = new Usuario({
      id: Date.now().toString(),
      ...data,
    });
    this.usuarios.push(usuario);
    return usuario;
  }

  /**
   * Atualiza um usuário.
   * @param {strin} id - ID do usuário a ser atualizado.
   * @param {object} data - Dados do usuário a ser atualizado.
   * @return {object} - Retorna o usuário atualizado.
   */
  async update(id: string, data: Partial<Usuario>) {
    let u: Usuario;

    const notExists = this.usuarios.every((usuario, i) => {
      if (usuario.id == id) {
        Object.assign(usuario[i], data);
        u = usuario[i];
        return false;
      }
      return true;
    });

    if (notExists) {
      throw new Error('Usuário não encontrado');
    }

    return u;
  }

  /**
   * Busca um usuário
   * @return {array} - Retorna os usuários encontrados.
   */
  async find() {
    return this.usuarios;
  }

  /**
   * Realiza a listagem de usuários
   * @param {object} options - Parâmetro adicional para a listagem de usuários
   * @return {array} - Retorna a listagem de usuários
   */
  async list(options?: ListUsuariosDTO): Promise<Usuario[]> {
    let data = this.usuarios.slice();

    if (options?.search) {
      let regex: RegExp;

      switch (options.search.type) {
        case 'startsWith':
          regex = new RegExp(`^${options.search.value}`);
          break;
        case 'endWith':
          regex = new RegExp(`${options.search.value}$`);
          break;
        case 'contains':
          regex = new RegExp(`^${options.search.value}$`);
          break;
      }

      if (options?.search?.fields && regex) {
        data = data.filter((usuario) =>
          !options.search.fields.every((field) => !regex.test(usuario[field])),
        );
      }
    }

    if (options?.sorting) {
      data = data.sort((a, b) =>
        Object.keys(options.sorting).reduce((bool, k) => bool || (
          (options.sorting[k] == 'asc' && (a[k] > b[k] ? 1 : -1)) ||
          (options.sorting[k] == 'desc' && (a[k] > b[k] ? -1 : 1))
        ), 0),
      );
    }

    if (options?.paging) {
      const offset = (options.paging.page - 1) * options.paging.size;
      const limit = (options.paging.page * options.paging.size);
      data = data.slice(offset, limit);
    }

    return data;
  }

  /**
   * Realiza a contagem de usuários
   * @param {object} options - Parâmetro adicional para a contagem de usuários.
   * @return {number} - Retorna a quantidade de usuários.
   */
  async count(options?: CountUsuariosDTO): Promise<number> {
    let data = this.usuarios.slice();

    if (options?.search) {
      let regex: RegExp;

      switch (options.search.type) {
        case 'startsWith':
          regex = new RegExp(`^${options.search.value}`);
          break;
        case 'endWith':
          regex = new RegExp(`${options.search.value}$`);
          break;
        case 'contains':
          regex = new RegExp(`^${options.search.value}$`);
          break;
      }

      if (options.search.fields && regex) {
        data = data.filter((usuario) =>
          !options.search.fields.every((field) => !regex.test(usuario[field])),
        );
      }
    }

    return data.length;
  }
}
