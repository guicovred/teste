import {Usuario} from '../../../domain/model/Usuario';
import {IUsuariosRepository} from '../../../domain/repositories/IUsuariosRepository';
import {CountUsuariosDTO} from '../../../dtos/CountUsuariosDTO';
import {ListUsuariosDTO} from '../../../dtos/ListUsuariosDTO';
import {UsuarioModel} from '../entities/UsuarioModel';
import {BaseRepository} from './BaseRepository';

/**
 * Repositórios de usuários utilizando o Mongoose
 */
export class MongooseUsuariosRepository implements IUsuariosRepository {
  private repository: BaseRepository

  /**
   * Criação de um novo usuário
   */
  constructor() {
    this.repository = new BaseRepository(UsuarioModel);
  }

  /**
   * Cria um usuário
   * @param {object} data - Dados para a criação de um usuário
   * @return {object} - Retorna usuário criado
   */
  async create(data: Omit<Usuario, 'id'>): Promise<Usuario> {
    const doc = await this.repository.create(data);
    return new Usuario(doc);
  }

  /**
   * Realiza a listagem de usuários
   * @param {object} options - Parâmetro adicional para a listagem de usuários
   * @return {array} - Retorna um array com a lista de usuários
   */
  async list(options?: ListUsuariosDTO): Promise<Usuario[]> {
    const docs = await this.repository.find({}, options);
    const data: Usuario[] = [];

    docs.forEach((doc) => {
      data.push(new Usuario(doc));
    });

    return data;
  }

  /**
   * Realiza a contagem de usuário
   * @param {object} options - Parâmetro adicional para a contagem de usuários
   * @return {number} - Retorna a quantidade de usuários
   */
  async count(options?: CountUsuariosDTO): Promise<number> {
    return this.repository.count(options);
  }
}
