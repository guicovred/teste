import {Context, Service, ServiceBroker} from 'moleculer';
import {CreateUsuarioService, ICreateUsuarioServiceRequest, ICreateUsuarioServiceResponse} from '../../../../domain/services/CreateUsuarioService';
import {IListUsuariosServiceRequest, IListUsuariosServiceResponse, ListUsuariosService} from '../../../../domain/services/ListUsuariosService';
import {MongooseUsuariosRepository} from '../../../mongoose/repositories/MongooseUsuariosRepository';

/**
 * Repositório de usuários utilizando Mongoose
 */
export default class GreeterService extends Service {
  readonly repository = new MongooseUsuariosRepository()

  /**
   * @param {object} broker - Parâmetro que irá lidar com a criação de usuário.
   */
  public constructor(public broker: ServiceBroker) {
    super(broker);

    this.parseServiceSchema({
      version: 1,
      name: 'usuarios',
      actions: {
        create: {
          async handler(ctx: Context<ICreateUsuarioServiceRequest>): Promise<ICreateUsuarioServiceResponse> {
            const service = new CreateUsuarioService(this.repository);
            return service.execute(ctx.params);
          },
        },
        list: {
          async handler(ctx: Context<IListUsuariosServiceRequest>): Promise<IListUsuariosServiceResponse> {
            const service = new ListUsuariosService(this.repository);
            return service.execute(ctx.params);
          },
        },
      },
    });
  }
}
