import {IPesquisa} from '@shared/domain/IPesquisa';

export interface CountAcoesDTO {
  search: IPesquisa;
}
