import {Acao} from '../../../domain/model/Acao';
import {IAcoesRepository} from '../../../domain/repositories/IAcoesRepository';
import {CountAcoesDTO} from '../../../dtos/CountAcoesDTO';
import {ListAcoesDTO} from '../../../dtos/ListAcoesDTO';
import {AcaoModel} from '../entities/AcaoModel';
import {BaseRepository} from './BaseRepository';
import {PapelModel} from '../../../../papeis/infra/mongoose/entities/PapelModel';
import {PermissaoModel} from '../../../../permissoes/infra/mongoose/entities/PermissaoModel';
import {UsuarioModel} from '../../../../usuarios/infra/mongoose/entities/UsuarioModel';


/**
 * Repositório das ações utilizando Mongoose
 */
export class MongooseAcoesRepository implements IAcoesRepository {
  private repository: BaseRepository

  /**
   *
   */
  constructor() {
    this.repository = new BaseRepository(AcaoModel);
  }

  /**
   * Cria uma ação
   * @param {object} data - Dados para a criação da ação.
   * @return {object} - Retorna a ação criada
   */
  async create(data: Omit<Acao, 'id'>): Promise<Acao> {
    const doc = await this.repository.create(data);
    return new Acao(doc);
  }

  /**
   * Lista as ações
   * @param {object} options - Opções a serem utilizadas na listagem de ações
   * @return {array} - Retorna a lista de ações
   */
  async list(options?: ListAcoesDTO): Promise<Acao[]> {
    const docs = await this.repository.find({}, options);
    const data: Acao[] = [];

    docs.forEach((doc) => {
      data.push(new Acao(doc));
    });

    return data;
  }

  /**
   * Realiza a contagem das ações
   * @param {object} options - Opções a serem utilizadas na listagem de ações
   * @return {number} - Retorna a quantidade de ações
   */
  async count(options?: CountAcoesDTO): Promise<number> {
    return this.repository.count(options);
  }

  /**
   *
   * @param {String} id - ID do usuário que terá suas ações retornadas.
   * @return {array} - Retorna um array com as ações do usuário.
   */
  async findByUserId(id: string): Promise<Acao[]> {
    const aggregate = UsuarioModel.aggregate([{$match: {_id: id}}]).lookup({
      'from': PapelModel.collection.collectionName,
      'localField': 'papeis',
      'foreignField': '_id',
      'as': 'papeis',
    }).lookup({
      'from': PermissaoModel.collection.collectionName,
      'localField': 'papeis.permissoes',
      'foreignField': '_id',
      'as': 'permissoes',
    }).lookup({
      'from': AcaoModel.collection.collectionName,
      'localField': 'permissoes.acoes',
      'foreignField': '_id',
      'as': 'acoes',
    });
    const docs = await aggregate.exec();
    const data: Acao[] = [];

    docs.acoes.forEach((doc) => {
      data.push(new Acao(doc));
    });

    return data;
  }
}
