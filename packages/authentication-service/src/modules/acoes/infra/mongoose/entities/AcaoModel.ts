import mongoose from 'mongoose';
import {Acao} from '../../../domain/model/Acao';

const opts: mongoose.SchemaOptions = {
  collection: 'acoes',
  toJSON: {
    virtuals: true,
  },
  timestamps: {},
};

const schema = new mongoose.Schema({
  chave: {
    type: String,
    required: true,
  },

  nivel: {
    type: String,
    required: true,
  },
}, opts);

/**
 * Modelo de uma ação
 */
export const AcaoModel = mongoose.model<Acao & mongoose.Document>('Acao', schema);
