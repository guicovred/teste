import {CountAcoesDTO} from '../../dtos/CountAcoesDTO';
import {ListAcoesDTO} from '../../dtos/ListAcoesDTO';
import {Acao} from '../model/Acao';
export interface IAcoesRepository {
    /**
      * Cria uma ação no sistema.
      * @param data - Dados da ação que será criada.
      * @returns Ação criada.
      */
    create(data: Omit<Acao, 'id'>): Promise<Acao>;

    /**
      * Lista as ações no sistema.
      * @param options - Parâmetro opcional para pesquisa, paginação e ordenação.
      * @returns Array contendo as ações.
      */
    list(options?: ListAcoesDTO): Promise<Acao[]>;

    /**
     * Conta as ações no sistema.
     * @param options - Parâmetro adicional para pesquisa.
     * @returns A quantidade de ações.
     */
    count(options?: CountAcoesDTO): Promise<number>;
}
