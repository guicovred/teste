/**
 * Representa uma ação na aplicação.
 */
export class Acao {
    id: string;
    nivel: string;
    chave: string;

    /**
     * Cria uma ação.
     * @param {Partial<Acao>} acao - Atributos da ação.
     */
    constructor(acao: Partial<Acao>) {
      this.id = acao.id;
      this.nivel = acao.nivel;
      this.chave = acao.chave;
    }
}
