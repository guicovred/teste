import faker from 'faker';
import {FakeAcoesRepository} from '../domain/repositories/fakes/FakeAcoesRepository';
import {ListAcoesService} from './ListAcoesService';

describe('ListAcaoService', () => {
  const repository = new FakeAcoesRepository();

  beforeEach(async () => {
    for (let i = 0; i < 20; i++) {
      await repository.create({
        chave: faker.name.findName(),
        nivel: faker.internet.email(),
      });
    }
  });

  it('should be able to list actions', async () => {
    const service = new ListAcoesService(repository);

    const result = await service.execute({
      page: 1,
      pageSize: 10,
    });

    expect(result).toBeDefined();
    expect(result.rows).toHaveLength(10);
    expect(result.total).toBe(20);
    expect(result.page).toBe(1);
    expect(result.totalPages).toBe(2);
  });
});
