import {FakeAcoesRepository} from '../domain/repositories/fakes/FakeAcoesRepository';
import {CreateAcaoService} from './CreateAcaoService';

describe('CreateAcaoService', () => {
  it('should be able to create an action', async () => {
    const repository = new FakeAcoesRepository();
    const service = new CreateAcaoService(repository);

    const acao = await service.execute({
      nivel: 'John Doe',
      chave: '12312312387',
    });

    expect(acao).toBeDefined();
    expect(acao).toHaveProperty('id');
    expect(acao.chave).toBe('12312312387');
    expect(acao.nivel).toBe('John Doe');
  });
});
