import {IOrdenacao} from '@shared/domain/IOrdenacao';
import {IPaginacao} from '@shared/domain/IPaginacao';
import {IPesquisa} from '@shared/domain/IPesquisa';

/**
 * Parâmetros opcionais para a listagem de permissões
 */
export interface ListPermissoesDTO {
  /**
   * Parâmetros de pesquisa
   */
  search: IPesquisa;
  /**
   * Parâmetros de paginação
   */
  paging: IPaginacao;
  /**
   * Parâmetros de ordenação
   */
  sorting: IOrdenacao;
}
