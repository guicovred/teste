import mongoose from 'mongoose';
import {Permissao} from '../../../domain/model/Permissao';

/**
 * Modelo de permissão
 */
const opts: mongoose.SchemaOptions = {
  collection: 'permissoes',
  toJSON: {
    virtuals: true,
  },
  timestamps: {},
};

const schema = new mongoose.Schema({
  chave: {
    type: String,
    required: true,
  },

  descricao: {
    type: String,
    required: true,
  },

  acoes: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Acao',
  }],
}, opts);

export const PermissaoModel = mongoose.model<Permissao & mongoose.Document>('Permissao', schema);
