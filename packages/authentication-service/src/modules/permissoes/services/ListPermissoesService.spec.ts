import faker from 'faker';
import {FakePermissoesRepository} from '../domain/repositories/fakes/FakePermissoesRepository';
import {ListPermissoesService} from './ListPermissoesService';

describe('ListPermissoesService', () => {
  const repository = new FakePermissoesRepository();

  beforeEach(async () => {
    for (let i = 0; i < 20; i++) {
      await repository.create({
        chave: faker.name.findName(),
        descricao: faker.internet.email(),
        acoes: [faker.name.findName()],
      });
    }
  });

  it('should be able to list permissions', async () => {
    const service = new ListPermissoesService(repository);

    const result = await service.execute({
      page: 1,
      pageSize: 10,
    });

    expect(result).toBeDefined();
    expect(result.rows).toHaveLength(10);
    expect(result.total).toBe(20);
    expect(result.page).toBe(1);
    expect(result.totalPages).toBe(2);
  });
});
