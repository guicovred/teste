import {FakePermissoesRepository} from '../domain/repositories/fakes/FakePermissoesRepository';
import {CreatePermissaoService} from './CreatePermissaoService';

describe('CreatePermissaoService', () => {
  it('should be able to create a permission', async () => {
    const repository = new FakePermissoesRepository();
    const service = new CreatePermissaoService(repository);

    const permissao = await service.execute({
      chave: 'John Doe',
      descricao: '12312312387',
      acoes: ['johndoe@foobar.com'],
    });

    expect(permissao).toBeDefined();
    expect(permissao).toHaveProperty('id');
    expect(permissao.chave).toBe('John Doe');
    expect(permissao.descricao).toBe('12312312387');
    expect(permissao.acoes).toContain('johndoe@foobar.com');
  })
})
