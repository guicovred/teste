import {IPermissoesRepository} from '../domain/repositories/IPermissoesRepository';

interface IRequest {
  chave: string;
  descricao: string;
  acoes?: string[];
}
interface IResponse {
  id: string;
  chave: string;
  descricao: string;
  acoes?: string[];
}

/**
  * Serviço de criação de permissões.
  */
export class CreatePermissaoService {
  private permissoesRepository: IPermissoesRepository;

  /**
   * @param {object} permissoesRepository - Repositório de permissões
   */
  constructor(permissoesRepository: IPermissoesRepository) {
    this.permissoesRepository = permissoesRepository;
  }

  /**
    * Efetua a criação de uma permissão.
    * @param {object} data - Dados da permissão.
    * @return {object} - Retorna a permissão criada.
    */
  async execute(data: IRequest): Promise<IResponse> {
    const permissao = await this.permissoesRepository.create(data);

    return {
      id: permissao.id,
      chave: permissao.chave,
      descricao: permissao.descricao,
      acoes: permissao.acoes,
    };
  }
}
