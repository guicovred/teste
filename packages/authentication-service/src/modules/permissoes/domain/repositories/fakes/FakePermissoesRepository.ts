import {Permissao} from '../../model/Permissao';
import {IPermissoesRepository} from '../IPermissoesRepository';
import {CountPermissoesDTO} from '../../../dtos/CountPermissoesDTO';
import {ListPermissoesDTO} from '../../../dtos/ListPermissoesDTO';

/**
 * Repositório falso de usuários utilizado para testes unitários
 */
export class FakePermissoesRepository implements IPermissoesRepository {
  private permissoes: Permissao[] = [];

  /**
   * Cria uma permissão.
   * @param {object} data - Dados a serem utilizados na criação da permissão.
   * @return {object} - Retorna a permissão criada.
   */
  async create(data: Omit<Permissao, 'id'>): Promise<Permissao> {
    const permissao = new Permissao({
      id: Date.now().toString(),
      ...data,
    });
    this.permissoes.push(permissao);
    return permissao;
  }

  /**
   * Atualiza uma permissão.
   * @param {string} id - ID da permissão que será atualizada.
   * @param {object} data - Dados da permissão que será atualizada.
   * @return {object} - Retorna a permissão atualizada.
   */
  async update(id: string, data: Partial<Permissao>) {
    let u: Permissao;

    const notExists = this.permissoes.every((permissao, i) => {
      if (permissao.id == id) {
        Object.assign(permissao[i], data);
        u = permissao[i];
        return false;
      }
      return true;
    });

    if (notExists) {
      throw new Error('Usuário não encontrado');
    }

    return u;
  }

  /**
   * Busca uma permissão.
   * @param {object} options - Parâmetros adicionais para a busca de permissão.
   * @return {object} - Retorna a permissão encontrada
   */
  async find() {
    return this.permissoes;
  }

  /**
   * Realiza a listagem de permissões
   * @param {object} options - Parâmetro opcional para a listagem de permissões.
   * @return {object} - Lista de permissões encontradas
   */
  async list(options?: ListPermissoesDTO): Promise<Permissao[]> {
    let data = this.permissoes.slice();


    if (options?.search) {
      let regex: RegExp;

      switch (options.search.type) {
        case 'startsWith':
          regex = new RegExp(`^${options.search.value}`);
          break;
        case 'endWith':
          regex = new RegExp(`${options.search.value}$`);
          break;
        case 'contains':
          regex = new RegExp(`^${options.search.value}$`);
          break;
      }

      if (options?.search?.fields && regex) {
        data = data.filter((permissao) =>
          !options.search.fields.every((field) =>
            !regex.test(permissao[field]),
          ),
        );
      }
    }

    if (options?.sorting) {
      data = data.sort((a, b) =>
        Object.keys(options.sorting).reduce((bool, k) => {
          return bool || (
            (options.sorting[k] == 'asc' && (a[k] > b[k] ? 1 : -1)) ||
            (options.sorting[k] == 'desc' && (a[k] > b[k] ? -1 : 1))
          );
        }, 0),
      );
    }

    if (options?.paging) {
      data = data.slice((options.paging.page - 1) * options.paging.size, options.paging.page * options.paging.size);
    }

    return data;
  }

  /**
   * Realiza a contagem de permissões
   * @param {object} options - Parâmetro opcional para a contagem de permissões.
   * @return {number} - Retorna a quantidade de permissões
   */
  async count(options?: CountPermissoesDTO): Promise<number> {
    let data = this.permissoes.slice();

    if (options?.search) {
      let regex: RegExp;

      switch (options.search.type) {
        case 'startsWith':
          regex = new RegExp(`^${options.search.value}`);
          break;
        case 'endWith':
          regex = new RegExp(`${options.search.value}$`);
          break;
        case 'contains':
          regex = new RegExp(`^${options.search.value}$`);
          break;
      }

      if (options.search.fields && regex) {
        data = data.filter((permissao) =>
          !options.search.fields.every((field) =>
            !regex.test(permissao[field]),
          ),
        );
      }
    }

    return data.length;
  }
}
