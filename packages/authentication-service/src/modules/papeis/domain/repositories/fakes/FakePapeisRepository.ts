import {Papel} from '../../model/Papel';
import {IPapeisRepository} from '../IPapeisRepository';
import {CountPapeisDTO} from '../../../dtos/CountPapeisDTO';
import {ListPapeisDTO} from '../../../dtos/ListPapeisDTO';

/**
 * Repositório falso de usuários utilizado para testes unitários
 */
export class FakePapeisRepository implements IPapeisRepository {
  private papeis: Papel[] = [];

  /**
   * Cria um papel
   * @param {object} data - Dados para a criação do papel.
   * @return {object} - Retorna o papel criado.
   */
  async create(data: Omit<Papel, 'id'>): Promise<Papel> {
    const papel = new Papel({
      id: Date.now().toString(),
      ...data,
    });
    this.papeis.push(papel);
    return papel;
  }

  /**
   * Atualiza um papel
   * @param {string} id - ID do papel a ser atualizado
   * @param {object} data - dados para a atulização do papel.
   * @return {object} - Retorna o papel atualizado.
   */
  async update(id: string, data: Partial<Papel>) {
    let u: Papel;

    const notExists = this.papeis.every((papel, i) => {
      if (papel.id == id) {
        Object.assign(papel[i], data);
        u = papel[i];
        return false;
      }
      return true;
    });

    if (notExists) {
      throw new Error('Usuário não encontrado');
    }

    return u;
  }

  /**
   * Busca um papel
   * @return {array} - Retorna um array com os papeis encontrados.
   */
  async find() {
    return this.papeis;
  }

  /**
   * Realiza a listagem de papeis
   * @param {object} options - Opções a serem utilizadas na listagem dos papeis.
   * @return {object} - Retorna a lista com os papeis encontrados.
   */
  async list(options?: ListPapeisDTO): Promise<Papel[]> {
    let data = this.papeis.slice();


    if (options?.search) {
      let regex: RegExp;

      switch (options.search.type) {
        case 'startsWith':
          regex = new RegExp(`^${options.search.value}`);
          break;
        case 'endWith':
          regex = new RegExp(`${options.search.value}$`);
          break;
        case 'contains':
          regex = new RegExp(`^${options.search.value}$`);
          break;
      }

      if (options?.search?.fields && regex) {
        data = data.filter((papel) =>
          !options.search.fields.every((field) =>
            !regex.test(papel[field]),
          ),
        );
      }
    }

    if (options?.sorting) {
      data = data.sort((a, b) =>
        Object.keys(options.sorting).reduce((bool, k) => {
          return bool || (
            (options.sorting[k] == 'asc' && (a[k] > b[k] ? 1 : -1)) ||
            (options.sorting[k] == 'desc' && (a[k] > b[k] ? -1 : 1))
          );
        }, 0),
      );
    }

    if (options?.paging) {
      data = data.slice((options.paging.page - 1) * options.paging.size, options.paging.page * options.paging.size);
    }

    return data;
  }

  /**
   * Realiza a contagem de papeis
   * @param {object} options - Opções a serem utilizadas na contagem de papeis.
   * @return {number} - Retorna a quantidade de papeis.
   */
  async count(options?: CountPapeisDTO): Promise<number> {
    let data = this.papeis.slice();

    if (options?.search) {
      let regex: RegExp;

      switch (options.search.type) {
        case 'startsWith':
          regex = new RegExp(`^${options.search.value}`);
          break;
        case 'endWith':
          regex = new RegExp(`${options.search.value}$`);
          break;
        case 'contains':
          regex = new RegExp(`^${options.search.value}$`);
          break;
      }

      if (options.search.fields && regex) {
        data = data.filter((papel) =>
          !options.search.fields.every((field) =>
            !regex.test(papel[field]),
          ),
        );
      }
    }

    return data.length;
  }
}
