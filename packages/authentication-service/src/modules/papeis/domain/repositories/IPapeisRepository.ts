import {CountPapeisDTO} from '../../dtos/CountPapeisDTO';
import {ListPapeisDTO} from '../../dtos/ListPapeisDTO';
import {Papel} from '../model/Papel';

export interface IPapeisRepository {

  /**
    * Cria um papel no sistema.
    * @param data - Dados do papel que será criado.
    * @returns Papel criado.
    */
  create(data: Omit<Papel, 'id'>): Promise<Papel>;

  /**
    * Lista os papeis no sistema.
    * @param options - Parâmetro opcional para pesquisa, paginação e ordenação.
    * @returns Array contendo os papeis.
    */
  list(options?: ListPapeisDTO): Promise<Papel[]>;

  /**
    * Conta os papeis do sistema.
    * @param options - Parâmetro opcional para pesquisa.
    * @returns A quantidade de papeis.
    */
  count(options?: CountPapeisDTO): Promise<number>;
}
