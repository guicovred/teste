import {IPapeisRepository} from '../domain/repositories/IPapeisRepository';

interface IRequest {
  chave: string;
  descricao: string;
  permissoes?: string[];
}
interface IResponse {
  id: string;
  chave: string;
  descricao: string;
  permissoes?: string[];
}

/**
  * Serviço de criação de papeis.
  */
export class CreatePapelService {
  private papeisRepository: IPapeisRepository;

  /**
   * Construtor dos papeis utilizando repositórios e interfaces
   * @param {object} papeisRepository - Repositório dos papeis
   */
  constructor(papeisRepository: IPapeisRepository) {
    this.papeisRepository = papeisRepository;
  }

  /**
    * Efetua a criação de um papel.
    * @param {object} data - Dados do papel.
    * @return {object} - O papel criado.
    */
  async execute(data: IRequest): Promise<IResponse> {
    const papel = await this.papeisRepository.create(data);

    return {
      id: papel.id,
      chave: papel.chave,
      descricao: papel.descricao,
      permissoes: papel.permissoes,
    };
  }
}
