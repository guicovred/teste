import faker from 'faker';
import {FakePapeisRepository} from '../domain/repositories/fakes/FakePapeisRepository';
import {ListPapeisService} from './ListPapeisService';

describe('ListPapelService', () => {
  const repository = new FakePapeisRepository();

  beforeEach(async () => {
    for (let i = 0; i < 20; i++) {
      await repository.create({
        chave: faker.name.findName(),
        descricao: faker.internet.email(),
        permissoes: [faker.name.findName()],
      });
    }
  });

  it('should be able to list roles', async () => {
    const service = new ListPapeisService(repository);

    const result = await service.execute({
      page: 1,
      pageSize: 10,
    });

    expect(result).toBeDefined();
    expect(result.rows).toHaveLength(10);
    expect(result.total).toBe(20);
    expect(result.page).toBe(1);
    expect(result.totalPages).toBe(2);
  });
});
