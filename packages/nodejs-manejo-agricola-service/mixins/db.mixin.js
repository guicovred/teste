"use strict";
const DbService = require("moleculer-db")
const MongooseAdapter = require("moleculer-db-adapter-mongoose")
const mongoose  = require("mongoose")
mongoose.set("runValidators", true)

module.exports = function (model) {
    // Mongo adapter

    return {
        mixins: [DbService],
        adapter: new MongooseAdapter(process.env.MONGOURI, {
            useUnifiedTopology: true
        }),
        model: model
    }
}