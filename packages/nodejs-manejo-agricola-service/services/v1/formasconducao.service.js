const { MoleculerError } = require("moleculer").Errors

const FormaConducao = require('../../models/FormaConducao')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(FormaConducao)],
    name: "formas-conducao",
    version: 1,

    settings: {
        fields: ["_id", "descricao", "ativo"],
    },
/*
    methods: {
        async checkRemove(ctx) {
            
            let agrupadores = await ctx.call("v1.XXXXXXXXXXXX.find", {
                query: {
                    idClasseToxicologica: ctx.params.id
                }
            })

            if (agrupadores && agrupadores.length)
                return Promise.reject(new MoleculerError("Não é possível excluir a forma de condução pois a mesma esta em uso", 401))
        }
    },

    hooks: {
        before: {
            remove: ["checkRemove"]
        }
    },
*/
}



