const { MoleculerError } = require("moleculer").Errors

const PrincipioAtivo = require('../../models/PrincipioAtivo')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(PrincipioAtivo)],
    name: "principios-ativos",
    version: 1,

    settings: {
        fields: ["_id", "descricao", "ativo"],
    },

    methods: {
        async checkRemove(ctx) {
            
            let agrupadores = await ctx.call("v1.agrupadores-produtos.find", {
                query: {
                    "principioAtivo.principioAtivo": ctx.params.id
                }
            })

            if (agrupadores && agrupadores.length)
                return Promise.reject(new MoleculerError("Não é possível excluir o princípio ativo pois o mesmo esta relacionado a algum agrupador de produto.", 401))            
        }
    },

    hooks: {
        before: {
            remove: ["checkRemove"]
        }
    },

}



