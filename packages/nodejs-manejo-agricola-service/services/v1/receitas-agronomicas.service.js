const { MoleculerError } = require("moleculer").Errors

const ReceitaAgronomica = require('../../models/ReceitaAgronomica')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(ReceitaAgronomica)],
    name: "receitas-agronomicas",
    version: 1,

    methods: {
        async carregarRelacoes(ctx) {

            if (ctx.params.filial) {
                ctx.params.filial = await ctx.call("v1.filiais.get", {
                    id: ctx.params.filial,
                    populate: "unidadeFederativa"
                })
            }

            if (ctx.params.cliente) {
                ctx.params.cliente = await ctx.call("v1.clientes.get", {
                    id: ctx.params.cliente,
                    populate: "uf"
                })
            }

            if (ctx.params.cultura) {
                ctx.params.cultura = await ctx.call("v1.culturas.get", {
                    id: ctx.params.cultura
                })
            }

            if (ctx.params.itens) {
                for (let i in ctx.params.itens) {
                    let item = ctx.params.itens[i]
                    if (item.agrupadorProduto) {
                        ctx.params.itens[i].agrupadorProduto = await ctx.call("v1.agrupadores-produtos.get", {
                            id: item.agrupadorProduto
                        })
                    }
                    if (item.diagnostico) {
                        ctx.params.itens[i].diagnostico = await ctx.call("v1.agrupadores-produtos.get", {
                            id: item.diagnostico
                        })
                    }
                }
            }
            return ctx
        }
    },

    hooks: {
        before: {
            update: ["carregarRelacoes"],
            create: ["carregarRelacoes"]
        }
    }
}



