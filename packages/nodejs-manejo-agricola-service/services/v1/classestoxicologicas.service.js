const { MoleculerError } = require("moleculer").Errors

const ClasseToxicologica = require('../../models/ClasseToxicologica')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(ClasseToxicologica)],
    name: "classes-toxicologicas",
    version: 1,

    settings: {
        fields: ["_id", "classe", "cor", "descricao", "ativo"],
    },

    methods: {
        async checkRemove(ctx) {

            let agrupadores = await ctx.call("v1.agrupadores-produtos.find", {
                query: {
                    classeToxicologica: ctx.params.id
                }
            })

            if (agrupadores && agrupadores.length)
                return Promise.reject(new MoleculerError("Não é possível excluir a classe pois a mesma esta em uso", 401))
        }
    },

    hooks: {
        before: {
            remove: ["checkRemove"]
        }
    },
}