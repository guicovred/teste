const { MoleculerError } = require("moleculer").Errors

const Segmento = require('../../models/Segmento')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(Segmento)],
    name: "segmentos",
    version: 1
}



