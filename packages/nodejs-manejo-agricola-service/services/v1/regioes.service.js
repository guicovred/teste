const { MoleculerError } = require("moleculer").Errors

const Regiao = require('../../models/Regiao')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(Regiao)],
    name: "regioes",
    version: 1
}



