const { MoleculerError } = require("moleculer").Errors

const PlantioTalhao = require('../../models/PlantioTalhao')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(PlantioTalhao)],
    name: "plantios-talhoes",
    version: 1,
//    
    settings: {
        populates: {
            "culturas": "v1.culturas.get",
        }
    },

}



