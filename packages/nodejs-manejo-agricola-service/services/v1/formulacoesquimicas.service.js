const { MoleculerError } = require("moleculer").Errors

const FormulacaoQuimica = require('../../models/FormulacaoQuimica')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(FormulacaoQuimica)],
    name: "formulacoes-quimicas",
    version: 1,

    settings: {
        fields: ["_id", "descricao", "ativo"],
    },

    methods: {
        async checkRemove(ctx) {
            
            let agrupadores = await ctx.call("v1.agrupadores-produtos.find", {
                query: {
                    formulacaoQuimica: ctx.params.id
                }
            })

            if (agrupadores && agrupadores.length)
                return Promise.reject(new MoleculerError("Não é possível excluir a formulação pois a mesma esta em uso", 401))
        }
    },

    hooks: {
        before: {
            remove: ["checkRemove"]
        }
    },
}



