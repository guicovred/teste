const { MoleculerError } = require("moleculer").Errors

const UnidadeMedida = require('../../models/UnidadeMedida')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(UnidadeMedida)],
    name: "unidades-medida",
    version: 1
/*
    methods: {
        async checkRemove(ctx) {
            
            let cadastroVinculado = await ctx.call("v1.?????.find", {
                query: {
                    unidadeMedida: ctx.params.id
                }
            })

            if (cadastroVinculado && cadastroVinculado.length)
                return Promise.reject(new MoleculerError("Não é possível excluir a unidade de medida pois o mesmo esta relacionado a algum ?.", 401))
        }
    },

    hooks: {
        before: {
            remove: ["checkRemove"]
        }
    }, 
    */
}



