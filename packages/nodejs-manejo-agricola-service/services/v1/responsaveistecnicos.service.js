const { MoleculerError } = require("moleculer").Errors

const ResponsavelTecnico = require('../../models/ResponsavelTecnico')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(ResponsavelTecnico)],
    name: "responsaveis-tecnicos",
    version: 1,

    settings: {
        fields: ["_id", "nomeCompleto", "CPF", "RG", "sexo", "endereco", "complemento", "bairro", "CEP", "cidade", "unidadeFederativa", "formacaoAcademica", "registroCREA", "registroCFTA", "telefone", "email", "anexoComplementar", "frasePadrao", "ativo", "filial"],
        populates: {
            "unidadeFederativa": "v1.unidades-federativas.get",
            "formacaoAcademica": "v1.formacoes-academicas.get",
            "filial": "v1.filiais.get",
        }
    },
}



