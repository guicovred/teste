const { MoleculerError } = require("moleculer").Errors

const AnotacaoResponsabilidadeTecnica = require('../../models/AnotacaoResponsabilidadeTecnica')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(AnotacaoResponsabilidadeTecnica)],
    name: "anotacoes-responsabilidades-tecnicas",
    version: 1,

    settings: {
        fields: ["_id", "responsavelTecnico", "registroCREA", "quantidadeReceitasART", "quantidadeReceitasMes", "numeroUltimaReceita", "numeroARTAtual", "numeroARTProxima", "omitirAssinaturaCliente", "ativo"],
        populates: {
            "responsavelTecnico": "v1.responsaveis-tecnicos.get",
        }
    },
}



