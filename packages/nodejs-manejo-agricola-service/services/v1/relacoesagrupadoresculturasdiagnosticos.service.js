const { MoleculerError } = require("moleculer").Errors

const RelacaoAgrupadorCulturaDiagnostico = require('../../models/RelacaoAgrupadorCulturaDiagnostico')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(RelacaoAgrupadorCulturaDiagnostico)],
    name: "relacoes-agrupadores-culturas-diagnosticos",
    version: 1,

    settings: {
        populates: {
            "agrupadorProduto"                  : "v1.agrupadores-produtos.get",
            "unidadeMedida"                     : "v1.unidades-medida.get",
            "formaConducao"                     : "v1.formas-conducao.get",
            "estagioCultura"                    : "v1.estagios-culturas.get",
            "estagioProblema"                   : "v1.estagios-problemas.get",
            "metodoAplicacao"                   : "v1.metodos-aplicacao.get",
            "modalidadeEquipamentoAplicacao"    : "v1.modalidades-equipamentos-aplicacoes.get",
            "estadosPermitidoVenda"             : "v1.unidades-federativas.get",
        }
    }
}