const { MoleculerError } = require("moleculer").Errors

const FormacaoAcademica = require('../../models/FormacaoAcademica')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(FormacaoAcademica)],
    name: "formacoes-academicas",
    version: 1,

    settings: {
        fields: ["_id", "descricao", "ativo"],
    },

    methods: {
        async checkRemove(ctx) {
            
            let responsavelTecnico = await ctx.call("v1.responsaveis-tecnicos.find", {
                query: {
                    formacaoAcademica: ctx.params.id
                }
            })

            if (responsavelTecnico && responsavelTecnico.length)
                return Promise.reject(new MoleculerError("Não é possível excluir a formação acadêmica pois a mesma esta em uso", 401))
        }
    },

    hooks: {
        before: {
            remove: ["checkRemove"]
        }
    },

}



