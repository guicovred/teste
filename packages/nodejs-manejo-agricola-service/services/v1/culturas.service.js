const { MoleculerError } = require("moleculer").Errors

const Cultura = require('../../models/Cultura')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(Cultura)],
    name: "culturas",
    version: 1,

    settings: {
        fields: ["_id", "nomePopular", "nomeCientifico", "valorCaldaPadrao", "codigoExterno", "ativo"],
    },

    methods: {
        async checkRemove(ctx) {
            
            let plantiotalhao = await ctx.call("v1.plantiotalhao.find", {
                query: {
                    culturas: ctx.params.id
                }
            })

            if (plantiotalhao && plantiotalhao.length)
                return Promise.reject(new MoleculerError("Não é possível excluir cultura pois a mesma esta em uso", 401))
        }
    },

    hooks: {
        before: {
            remove: ["checkRemove"]
        }
    },
}



