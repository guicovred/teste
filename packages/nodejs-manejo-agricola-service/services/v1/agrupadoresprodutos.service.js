const { MoleculerError } = require("moleculer").Errors

const AgrupadorProdutos = require('../../models/AgrupadorProdutos')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(AgrupadorProdutos)],
    name: "agrupadores-produtos",
    version: 1,

    settings: {
        fields: ["_id", "nomeComercial", "codigoExterno", "grupoQuimico", "titularRegistro", "registroMapa", "formulacaoQuimica", "classeToxicologica", "principioAtivo", "ufVendaPermitida", "ativo"],
        populates: {
            "ufVendaPermitida": "v1.unidades-federativas.get",
            "principioAtivo.id": "v1.principios-ativos.get",
            "classeToxicologica": "v1.classes-toxicologicas.get",
            "formulacaoQuimica": "v1.formulacoes-quimicas.get",
        }
    },
}



