const { MoleculerError } = require("moleculer").Errors

const RecomendacaoGenerica = require('../../models/RecomendacaoGenerica')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(RecomendacaoGenerica)],
    name: "recomendacoes-genericas",
    version: 1,

    settings: {
        fields: ["_id", "descricao", "equipamentoProtecao", "manejoPragasResistencias", "disposicaoResiduosEmbalagens", "frasePadrao", "ativo"],
    },

}



