const { MoleculerError } = require("moleculer").Errors

const UnidadeFederativa = require('../../models/UnidadeFederativa')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(UnidadeFederativa)],
    name: "unidades-federativas",
    version: 1,

    settings: {
        fields: ["_id", "descricao", "uf"],
    },
}



