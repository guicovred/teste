const { MoleculerError } = require("moleculer").Errors

const ModalidadeEquipamentoAplicacao = require('../../models/ModalidadeEquipamentoAplicacao')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(ModalidadeEquipamentoAplicacao)],
    name: "modalidades-equipamentos-aplicacoes",
    version: 1,

    settings: {
        fields: ["_id", "descricao", "ativo"],
    },
/*
    methods: {
        async checkRemove(ctx) {
            
            let agrupadores = await ctx.call("v1.XXXXXXXXXXXXXXXX.find", {
                query: {
                    idClasseToxicologica: ctx.params.id
                }
            })

            if (agrupadores && agrupadores.length)
                return Promise.reject(new MoleculerError("Não é possível excluir a classe pois a mesma esta em uso", 401))
        }
    },

    hooks: {
        before: {
            remove: ["checkRemove"]
        }
    },
*/
}



