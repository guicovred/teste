const { MoleculerError } = require("moleculer").Errors

const Diagnostico = require('../../models/Diagnostico')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(Diagnostico)],
    name: "diagnosticos",
    version: 1,

    hooks: {
        before: {
            find: ["processarParametros"],
            list: ["processarParametros"],
        }
    },
    methods: {
        async processarParametros(ctx) {

            if (ctx.params.query) {

                let {cultura, agrupadorProduto, ...query} = ctx.params.query
                
                let relacoes = await ctx.call("v1.relacoes-agrupadores-culturas-diagnosticos.find", {
                    query: {
                        $or: [
                            {
                                "cultura._id": cultura
                            }, 
                            {
                                "agrupadorProduto._id": agrupadorProduto
                            }
                        ],       
                        ativo: true,
                    }
                })

                ctx.params.query = {
                    _id: relacoes.map(obj => obj.diagnostico._id),
                    ...query                    
                }
            }
            return ctx
        }

    },

    settings: {
        fields: ["_id", "descricao", "ativo"],
    },



    /*
        methods: {
            async checkRemove(ctx) {
                
                let agrupadores = await ctx.call("v1.XXXXXXXXXXXXXXXX.find", {
                    query: {
                        idClasseToxicologica: ctx.params.id
                    }
                })
    
                if (agrupadores && agrupadores.length)
                    return Promise.reject(new MoleculerError("Não é possível excluir a classe pois a mesma esta em uso", 401))
            }
        },
    
        hooks: {
            before: {
                remove: ["checkRemove"]
            }
        },
    */
}