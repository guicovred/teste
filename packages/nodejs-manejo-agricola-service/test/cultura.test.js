var assert = require('assert')
const { ServiceBroker } = require('moleculer')


describe('v1.culturas', () => {

    const model = {
        nomePopular: 'Teste Cultura',
        nomeCientifico: 'Teste Cultura cientifico',
        valorCaldaPadrao: 3,
        codigoExterno: '04',
    }

    let broker = new ServiceBroker({ logger: false })

    broker.loadServices("services", "**/*.service.js")

    beforeAll(() => broker.start());
    afterAll(() => broker.stop());

    describe('v1.culturas.create', () => {

        it('cria e salva cultura com sucesso', async () => {
            let temp = Object.assign({}, model)
            let created = await broker.call('v1.culturas.create', temp)

            expect(created).toBeDefined()
            expect(created.valorCaldaPadrao).toBe(model.valorCaldaPadrao)
            expect(created.nomePopular).toBe(model.nomePopular)
            expect(created.nomeCientifico).toBe(model.nomeCientifico)

            await broker.call('v1.culturas.remove', { id: created._id })
        })

        it('dispara erro ao criar cultura com campos obrigatórios faltando', async () => {
            let temp = Object.assign({}, model)

            delete temp.nomePopular

            await expect(broker.call('v1.culturas.create', temp)).rejects.toThrow()
        })
    })

    describe('v1.usuario.get', () => {
        it('pesquisa usuário sem passar um login', async () => {
            await expect(broker.call('v1.culturas.get')).rejects.toThrow()
        })

        it('pesquisa usuário por login existente', async () => {
            let temp = Object.assign({}, model)
            let created = await broker.call('v1.culturas.create', temp)

            let result = await broker.call('v1.culturas.get', { login: created.login })

            expect(result).toBeDefined()
            expect(result.login).toBe(created.login)
            expect(result.nome).toBe(created.nome)
            expect(result.email).toBe(created.email)

            await broker.call('v1.culturas.remove', { id: created._id })
        })

        it('pesquisa usuário por login inexistente', async () => {
            await expect(broker.call('v1.culturas.get', { login: 'usuario_inexistente' })).rejects.toThrow()
        })
    })


    describe('v1.culturas.update', () => {

        it('atualiza um campo do cultura', async () => {
            let temp = Object.assign({}, model)
            let created = await broker.call('v1.culturas.create', temp)

            let nomeCientifico = 'Teste Cientifico'

            let updated = await broker.call('v1.culturas.update', { id: created._id, nomeCientifico: nomeCientifico })

            expect(updated).toBeDefined()
            expect(updated.valorCaldaPadrao).toBe(created.valorCaldaPadrao)
            expect(updated.nomePopular).toBe(created.nomePopular)
            expect(updated.nomeCientifico).toBe(nomeCientifico)

            await broker.call('v1.culturas.remove', { id: created._id })
        })

    })

    describe('v1.culturas.remove', () => {
        it('exclui cultura', async () => {
            let temp = Object.assign({}, model)
            let created = await broker.call('v1.culturas.create', temp)
            let removed = await broker.call('v1.culturas.remove', { id: created._id })

            expect(removed).toBeDefined()
            expect(removed).toEqual(created)
        })

        it('tenta excluir cultura sem passar um id', async () => {
            await expect(broker.call('v1.culturas.remove')).rejects.toThrow()
        })

        it('tenta excluir cultura passando um id que não existe', async () => {
            await expect(broker.call('v1.culturas.remove', { id: 'usuario_inexistente' })).rejects.toThrow()
        })
    })
})