const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const CulturaSchema = new mongoose.Schema({
    
    ativo: {
        type: Boolean,
        default: true
    },
    nomePopular: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    },
    nomeCientifico: {
        type: String,
    },
    valorCaldaPadrao: {
        type: Number,
        default: 0,
    },
    codigoExterno: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    }
}, opts)

CulturaSchema.index({
    codigoExterno: 'text',
    nomePopular: 'text',
    nomeCientifico: 'text'
});

module.exports = mongoose.model("Cultura", CulturaSchema);