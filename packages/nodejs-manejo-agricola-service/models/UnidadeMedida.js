const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const UnidadeMedidaSchema = new mongoose.Schema({
    descricao: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    },
    abreviacao: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    }
}, opts)

UnidadeMedidaSchema.index({
    descricao: 'text',
    abreviacao: 'text'
});

module.exports = mongoose.model("UnidadeMedida", UnidadeMedidaSchema);