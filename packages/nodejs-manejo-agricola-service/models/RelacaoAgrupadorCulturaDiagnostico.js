const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const RelacaoAgrupadorCulturaDiagnosticoSchema = new mongoose.Schema({
    _id: {
        type: String
    },
    agrupadorProduto: {
        _id: {
            type: String,
            required: true
        },
        nomeComercial: {
            type: String,
            required: true
        }
    },
    cultura: {
        _id: {
            type: String,
            required: true
        },
        nomePopular: {
            type: String,
            required: true
        }
    },
    diagnostico: {
        _id: {
            type: String,
            required: true
        },
        descricao: {
            type: String,
            required: true
        }
    },
    ativo: {
        type: Boolean,
        default: true
    },
    dosagemMinima: {
        type: Number,
    },
    dosagemMaxima: {
        type: Number,
    },
    area: {       
        type: Number,
    },
    unidadeMedida: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'UnidadeMedida'
    },
    carencia: {       
        type: Number,
    },
    caldaMinima: {       
        type: Number,
    },
    caldaMaxima: {       
        type: Number,
    },
    quantidadeAplicacoesMinima: {       
        type: Number,
    },
    quantidadeAplicacoesMaxima: {       
        type: Number,
    },
    formaConducao: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'FormaConducao'
    },
    estagioCultura: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'EstagioCultura'
    },
    estagioProblema: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'EstagioProblema'
    },
    metodoAplicacao: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'MetodoAplicacao'
    },
    modalidadeEquipamentoAplicacao: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'ModalidadeEquipamentoAplicacao'
        }
    ],
    epocaAplicacao: {       
        type: String,
    },
    precaucoesUso: {       
        type: String,
    },
    incompatibilidade: {       
        type: String,
    },
    equipamentoProtecaoObrigatorio: {       
        type: String,
    },
    manejoIntegradoPragasResistencia: {       
        type: String,
    },
    disposicaoFinalResiduosEmbalagem: {       
        type: String,
    },
    frasePadrao: {       
        type: String,
    },
    estadosPermitidoVenda: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'UnidadeFederativa'
        }
    ],
    formaCalculo: {
        type: String,
        required: true
    },
    diluicao: {       
        type: Number,
        required: true
    },

}, opts)


module.exports = mongoose.model("RelacaoAgrupadorCulturaDiagnostico", RelacaoAgrupadorCulturaDiagnosticoSchema);