const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const FormulacaoQuimicaSchema = new mongoose.Schema({
    
    ativo: {
        type: Boolean,
        default: true
    },
    descricao: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    }
}, opts)

FormulacaoQuimicaSchema.index({
    descricao: 'text'
});

module.exports = mongoose.model("FormulacaoQuimica", FormulacaoQuimicaSchema);