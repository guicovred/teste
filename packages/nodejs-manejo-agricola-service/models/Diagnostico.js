const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const DiagnosticoSchema = new mongoose.Schema({
    ativo: {
        type: Boolean,
        default: true
    },
    descricao: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    }
}, opts)

DiagnosticoSchema.index({
    descricao: 'text'
});

module.exports = mongoose.model("Diagnostico", DiagnosticoSchema);