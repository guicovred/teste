const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const ResponsavelTecnicochema = new mongoose.Schema({
    
    ativo: {
        type: Boolean,
        default: true
    },
    nomeCompleto: {
        type: String,
        required: true,
    },
    CPF: {
        type: String,
        required: true,
        index: {
            unique: true,
        }
    },
    RG: {
        type: String,
        required: true,
    },
    sexo: {
        type: String,
        required: true,
    },
    endereco: {
        type: String,
        required: true,
    },
    complemento: {
        type: String,
    },
    bairro: {
        type: String,
        required: true,
    },
    CEP: {
        type: String,
        required: true,
    },
    cidade: {
        type: String,
        required: true,
    },
    unidadeFederativa:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'UnidadeFederativa'
    },
    formacaoAcademica:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'FomacaoAcademica'
    },
    filial:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Filial'
    },
    registroCREA: {
        type: String,
    },
    registroCFTA: {
        type: String,
    },
    telefone: {
        type: Number,
    },
    email: {
        type: String,
        index: {
            unique: true,
            required: true,
        }
    },
    anexoComplementar: {
        type: String,
    },
    frasePadrao: {
        type: String,
    },

}, opts)

ResponsavelTecnicochema.index({
    nomeCompleto: 'text',
    CPF: 'text',
    email: 'text'
});


module.exports = mongoose.model("ResponsavelTecnico", ResponsavelTecnicochema);