const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const SegmentoSchema = new mongoose.Schema({
    descricao: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    },
    codigoExterno: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    }
}, opts)

SegmentoSchema.index({
    codigoExterno: 'text',
    descricao: 'text'
});

module.exports = mongoose.model("Segmento", SegmentoSchema);