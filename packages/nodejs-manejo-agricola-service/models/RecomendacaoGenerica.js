const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const RecomendacaoGenericaSchema = new mongoose.Schema({
    
    ativo: {
        type: Boolean,
        default: true
    },
    descricao: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    },
    equipamentoProtecao: {
        type: String,
    },
    manejoPragasResistencias: {
        type: String,
    },
    disposicaoResiduosEmbalagens: {
        type: String,
    },
    frasePadrao: {
        type: String,
    }
}, opts)

RecomendacaoGenericaSchema.index({
    descricao: 'text'
});

module.exports = mongoose.model("RecomendacaoGenerica", RecomendacaoGenericaSchema);