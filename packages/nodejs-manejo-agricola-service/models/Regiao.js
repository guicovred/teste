const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const RegiaoSchema = new mongoose.Schema({
    descricao: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    },
    codigoExterno: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    }
}, opts)

RegiaoSchema.index({
    codigoExterno: 'text',
    descricao: 'text'
});

module.exports = mongoose.model("Regiao", RegiaoSchema);