const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const ReceitaAgronomicaSchema = new mongoose.Schema({

    codigoExterno: {
        type: String,
        required: true,
    },
    filial: {
        codigoExterno: {
            type: String,
        },
        razaoSocial: {
            type: String,
        },
        nomeFantasia: {
            type: String,
        },
        endereco: {
            type: String,
        },
        bairro: {
            type: String,
        },
        cidade: {
            type: String,
        },
        unidadeFederativa: {
            descricao: {
                type: String,
            },
            uf: {
                type: String,
            }
        },
    },
    numero: {
        type: Number,
    },
    cliente: {
        _id: {
            type: mongoose.Schema.Types.ObjectId,
            required: true
        },
        nome: {
            type: String,
        },
        endereco: {
            type: String,
        },
        cep: {
            type: String,
        },
        municipio: {
            type: String,
        },
        bairro: {
            type: String,
        },
        cpfcnpj: {
            type: String,
        },
        uf: {
            descricao: {
                type: String,
            },
            uf: {
                type: String,
            }
        }
    },
    cultura: {
        _id: {
            type: mongoose.Schema.Types.ObjectId,
            required: true
        },
        nomePopular: {
            type: String,
        },
        nomeCientifico: {
            type: String,
        },
        estagio: {
            type: String,
        }
    },
    status: {
        type: String,
    },
    data: {
        type: Date,
    },
    itens: [{
        item: {
            type: Number,
            require: true
        },
        agrupadorProduto: {
            _id: {
                type: mongoose.Schema.Types.ObjectId,
                required: true
            },
            nomeComercial: {
                type: String,
            },
            principioAtivo: [{
                descricao: {
                    type: String,
                },
                concentracao: {
                    type: Number,
                }
            }],
        },
        diagnostico: {
            _id: {
                type: mongoose.Schema.Types.ObjectId
            },
            descricao: {
                type: String,
            },
            estagio: {
                type: String,
            }
        },
        quantidade: {
            type: Number,
            required: true,
        },
        quantidadeAplicacoes: {
            type: Number,
        },
        dose: {
            type: String,
        },
        calda: {
            type: String,
        },
        epocaAplicacao: {
            type: String,
        }
    }],
}, opts)

ReceitaAgronomicaSchema.index({
    codigoExterno: 'text'
});


module.exports = mongoose.model("ReceitaAgronomica", ReceitaAgronomicaSchema);