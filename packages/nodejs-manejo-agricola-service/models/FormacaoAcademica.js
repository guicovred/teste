const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const FormacaoAcademicaSchema = new mongoose.Schema({
    
    ativo: {
        type: Boolean,
        default: true
    },
    descricao: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    }
}, opts)

FormacaoAcademicaSchema.index({
    descricao: 'text'
});


module.exports = mongoose.model("FormacaoAcademica", FormacaoAcademicaSchema);