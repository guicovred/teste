const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const FormaConducaoSchema = new mongoose.Schema({
    
    ativo: {
        type: Boolean,
        default: true
    },
    descricao: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    }
}, opts)

FormaConducaoSchema.index({
    descricao: 'text'
});

module.exports = mongoose.model("FormaConducao", FormaConducaoSchema);