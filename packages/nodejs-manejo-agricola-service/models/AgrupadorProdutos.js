const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const AgrupadorProdutoschema = new mongoose.Schema({

    ativo: {
        type: Boolean,
        default: true
    },
    nomeComercial: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    },
    codigoExterno: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    },
    grupoQuimico: {
        type: String,
    },
    titularRegistro: {
        type: String,
    },
    registroMapa: {
        type: String,
    },
    formulacaoQuimica: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'FormulacaoQuimica'
    },
    classeToxicologica: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ClasseToxicologica'
    },
    principioAtivo: [{
        ativo: {
            type: Boolean,
            default: true
        },
        descricao: {
            type: String,
            required: true,
            index: {
                unique: true,
            },
        },
        concentracao: {
            type: Number,
        }
    }],

    ufVendaPermitida: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'UnidadeFederativa'
        }
    ]
    //fichaEmergencia: 
}, opts)

AgrupadorProdutoschema.index({
    codigoExterno: 'text',
    nomeComercial: 'text'
});


module.exports = mongoose.model("AgrupadorProdutos", AgrupadorProdutoschema);