const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const UnidadeFederativaSchema = new mongoose.Schema({
    descricao: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    },
    uf: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    }
}, opts)

UnidadeFederativaSchema.index({
    uf: 'text',
    descricao: 'text'
});

module.exports = mongoose.model("UnidadeFederativa", UnidadeFederativaSchema);