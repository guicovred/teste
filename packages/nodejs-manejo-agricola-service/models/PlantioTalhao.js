const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const PlantioTalhaoSchema = new mongoose.Schema({
    
    ativo: {
        type: Boolean,
        default: true
    },
    descricao: {
        type: String,
        required: true,
    },
    culturas: [
        {       
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Cultura'
        }
    ],
    area: {
        type: Number,
    },
    latitude: {
        type: String,
    },
    longitude: {
        type: String,
    }
}, opts)

PlantioTalhaoSchema.index({
    descricao: 'text'
});

module.exports = mongoose.model("PlantioTalhao", PlantioTalhaoSchema)