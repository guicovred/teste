const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const EstagioCulturaSchema = new mongoose.Schema({
    
    ativo: {
        type: Boolean,
        default: true
    },
    descricao: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    }
}, opts)

EstagioCulturaSchema.index({
    descricao: 'text'
});


module.exports = mongoose.model("EstagioCultura", EstagioCulturaSchema);