const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const PrincipioAtivoSchema = new mongoose.Schema({
    
    ativo: {
        type: Boolean,
        default: true
    },
    descricao: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    }
}, opts)

PrincipioAtivoSchema.index({
    descricao: 'text',
});

module.exports = mongoose.model("PrincipioAtivo", PrincipioAtivoSchema);