const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const MetodoAplicacaoSchema = new mongoose.Schema({
    
    ativo: {
        type: Boolean,
        default: true
    },
    descricao: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    }
}, opts)

MetodoAplicacaoSchema.index({
    descricao: 'text'
});

module.exports = mongoose.model("MetodoAplicacao", MetodoAplicacaoSchema);