const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const ClasseToxicologicaSchema = new mongoose.Schema({
    ativo: {
        type: Boolean,
        default: true
    },
    classe: {
        type: String,
        required: true,
    },
    cor: {
        codigo: {
            type: Object,
            required: true,
        },
        descricao: {
            type: Object,
            required: true,
        }
    },
    descricao: {
        type: String,
        required: true,
        index: {
            unique: true,
        }
    }
}, opts)


ClasseToxicologicaSchema.index({
    classe: 'text',
    descricao: 'text'
});

module.exports = mongoose.model("ClasseToxicologica", ClasseToxicologicaSchema);