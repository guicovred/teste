const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const AnotacaoResponsabilidadeTecnicaSchema = new mongoose.Schema({
    
    ativo: {
        type: Boolean,
        default: true
    },
    responsavelTecnico: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ResponsavelTecnico',
        required: true,
    },
    registroCREA: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    },
    quantidadeReceitasART: {
        type: Number,
        default: 0
    },
    quantidadeReceitasMes: {
        type: Number,
        default: 0
    },
    numeroUltimaReceita: {
        type: Number,
        default: 0
    },
    numeroARTAtual: {       
        type: Number,
        default: 0
    },
    numeroARTProxima: {
        type: Number,
        default: 0
    },
    omitirAssinaturaCliente: {
        type: String,
        default: false
    },
}, opts)

AnotacaoResponsabilidadeTecnicaSchema.index({
    responsavelTecnico: 'text',
    registroCREA: 'text'
});

module.exports = mongoose.model("AnotacaoResponsabilidadeTecnica", AnotacaoResponsabilidadeTecnicaSchema)