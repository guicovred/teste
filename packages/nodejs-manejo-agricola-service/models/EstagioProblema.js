const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const EstagioProblemaSchema = new mongoose.Schema({
    
    ativo: {
        type: Boolean,
        default: true
    },
    descricao: {
        type: String,
        required: true,
        index: {
            unique: true,
        },
    }
}, opts)

EstagioProblemaSchema.index({
    descricao: 'text'
});


module.exports = mongoose.model("EstagioProblema", EstagioProblemaSchema);