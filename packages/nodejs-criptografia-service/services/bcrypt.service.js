const bcrypt = require('bcryptjs')
const { MoleculerError } = require("moleculer").Errors

module.exports = {
    name: "bcrypt",
    version: 1,
    actions: {
        async hash(ctx) {
            if (!ctx.params.texto)
                return Promise.reject(new MoleculerError("Texto não informado", 500))

            if (!ctx.params.saltFactor)
                return Promise.reject(new MoleculerError("Salt factor não informado", 500))

            let salt = await bcrypt.genSalt(ctx.params.saltFactor)
            let hash = await bcrypt.hash(ctx.params.texto, salt)

            return Promise.resolve(hash)
        },

        async compare(ctx) {
            if (!ctx.params.texto)
                return Promise.reject(new MoleculerError("Texto não informado", 500))

            if (!ctx.params.hash)
                return Promise.reject(new MoleculerError("Hash não informada", 500))

            let result = await bcrypt.compare(ctx.params.texto, ctx.params.hash)

            return Promise.resolve(result)
        }
    }
}