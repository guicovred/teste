const { v4: uuidv4 } = require('uuid');
const { MoleculerError } = require("moleculer").Errors

module.exports = {
    name: "uuid",
    version: 1,
    actions: {
        uuidv4(ctx) {
            return uuidv4()
        }
    }
}