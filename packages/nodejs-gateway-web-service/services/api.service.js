const ApiGateway = require("moleculer-web");
const { MoleculerError } = require("moleculer").Errors
var cookieParser = require('cookie-parser')
var helmet = require('helmet')
var compression = require('compression')

module.exports = {
    mixins: [ApiGateway],
    name: "gateway",
    version: 1,
    settings: {
        use: [
            cookieParser(),
            helmet(),
            compression()
        ],
        path: "/api",
        cors: {
            origin: ["http://receituario.controlesuascoisas.com.br:8080", "http://receituario.controlesuascoisas.com.br:8081", "http://192.168.3.215:8080", "http://192.168.3.215:8081", "http://api.receituario:8080", "http://api.receituario:8081"],
            credentials: true
        },
        routes: [
            {
                whitelist: [
                    "$node.*",
                ]
            },

            {

                path: "gateway",
                whitelist: [
                    "v1.gateway.*"
                ],
                aliases: {
                    "GET /list-aliases": "v1.gateway.listAliases"
                }
            },

            {
                path: "/v1",
                //authorization: true,               
                aliases: {
                    "REST /usuarios": "v1.usuarios",
                    "REST /papeis": "v1.papeis",
                    "REST /permissoes": "v1.permissoes",
                    "REST /credenciais": "v1.credenciais",
                    "REST /credenciais-usuario": "v1.credenciais-usuario",
                    "REST /credenciais-sistema": "v1.credenciais-sistema",

                    "REST /clientes": "v1.clientes",

                    "REST /receitas-agronomicas": "v1.receitas-agronomicas",

                    "REST /culturas": "v1.culturas",
                    "REST /segmentos": "v1.segmentos",
                    "REST /regioes": "v1.regioes",
                    "REST /agrupadores-produtos": "v1.agrupadores-produtos",
                    "REST /filiais": "v1.filiais",
                    "REST /formulacoes-quimicas": "v1.formulacoes-quimicas",
                    "REST /classes-toxicologicas": "v1.classes-toxicologicas",
                    "REST /diagnosticos": "v1.diagnosticos",
                    "REST /unidades-federativas": "v1.unidades-federativas",
                    "REST /formacoes-academicas": "v1.formacoes-academicas",
                    "REST /principios-ativos": "v1.principios-ativos",
                    "REST /recomendacoes-genericas": "v1.recomendacoes-genericas",
                    "REST /plantios-talhoes": "v1.plantios-talhoes",
                    "REST /unidades-medida": "v1.unidades-medida",
                    "REST /formas-conducao": "v1.formas-conducao",
                    "REST /estagios-culturas": "v1.estagios-culturas",
                    "REST /estagios-problemas": "v1.estagios-problemas",
                    "REST /metodos-aplicacao": "v1.metodos-aplicacao",
                    "REST /modalidades-equipamentos-aplicacoes": "v1.modalidades-equipamentos-aplicacoes",
                    "REST /anotacoes-responsabilidades-tecnicas": "v1.anotacoes-responsabilidades-tecnicas",
                    "REST /responsaveis-tecnicos": "v1.responsaveis-tecnicos",
                    "REST /relacoes-agrupadores-culturas-diagnosticos": "v1.relacoes-agrupadores-culturas-diagnosticos",
                },
                bodyParsers: {
                    json: { limit: "2MB" },
                    urlencoded: { extended: true, limit: "2MB" }
                }
            },
            {
                path: "/v1/autenticacao",
                authentication: true,
                aliases: {
                    "PUT /usuario": "v1.autenticacao.updateUsuario",
                    "GET /usuario": "v1.autenticacao.getUsuario",
                    "POST /refresh-token": "v1.autenticacao.refreshToken",
                    "POST /resume-session": "v1.autenticacao.resumeSession",
                    "POST /logout": "v1.autenticacao.logout"
                },
                bodyParsers: {
                    json: { limit: "2MB" },
                    urlencoded: { extended: true, limit: "2MB" }
                }
            },
            {
                path: "/v1/autenticacao",
                aliases: {
                    "POST /login": "v1.autenticacao.login"
                },
                bodyParsers: {
                    json: { limit: "2MB" },
                    urlencoded: { extended: true, limit: "2MB" }
                },
            },
        ]
    },

    methods: {
        async authenticate(ctx, route, req, res) {
            if (req.cookies.USER_SESSION) {
                let token = req.cookies.USER_SESSION
                let decoded = await ctx.call("v1.jwt.decode", { token })

                let sessao = null

                try {
                    sessao = await ctx.call("v1.sessoes.get", {
                        id: decoded.id
                    })

                } catch (erro) {
                    if (erro.code != 404) throw erro
                }

                if (!sessao || !sessao.ativa || sessao.uuid !== decoded.uuid) {
                    throw new MoleculerError("Sessão expirada ou inexistente", 401)
                }

                ctx.meta.sessao = sessao._id.toString()
                ctx.meta.credencial = sessao.credencial.toString()


                // O retorno preenche o campo 
                // ctx.meta.user com o ID do 
                // usuário da sessão
                return sessao.usuario.toString()
            }

            return null
        },

        async authorize(ctx, route, req, res) {

            let credencial = await ctx.call("v1.autenticacao.get", {
                id: ctx.meta.credencial
            })

            if (!ctx.meta.credencial) {
                throw new MoleculerError("Credencial não encontrada", 401);
            }

            let permissoes = await ctx.call("v1.permissoes.find", {
                query: {
                    acao: acao,
                    papeis: credencial.papeis
                }
            })

            if (!permissoes || !permissoes.length)
                throw new MoleculerError("Permissão negada", 403);

            ctx.meta.credencial = credencial

            return ctx;
        }
    }
}