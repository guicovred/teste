"use strict";
const DbService = require("moleculer-db")
const MongooseAdapter = require("moleculer-db-adapter-mongoose")

module.exports = function (model) {
    // Mongo adapter

    return {
        mixins: [DbService],
        adapter: new MongooseAdapter(process.env.MONGOURI, {
            useUnifiedTopology: true
        }),
        model: model,
        hooks: {
            before: {
                "*": (ctx) => {
                    if (ctx.params.query) {
                        for (let k in ctx.params.query) {
                            if (typeof ctx.params.query[k] === "string") {
                                if (ctx.params.query[k] !== "true" && ctx.params.query[k] !== "false") {
                                    ctx.params.query[k] = new RegExp(ctx.params.query[k], "i")
                                }
                            }
                        }
                    }
                },
            }
        }
    }
}
