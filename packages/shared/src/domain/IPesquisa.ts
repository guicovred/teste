/**
 * Parâmetros genéricos utilizados para pesquisa no sistema
 */
export interface IPesquisa {
  /**
   * Campos onde a pesquisa deverá ser feita
   */
  fields: string[];
  /**
   * Tipo de pesquisa a ser feita
   */
  type: 'startsWith' | 'endWith' | 'contains'
  /**
   * Valor a ser pesquisado
   */
  value: string;
}