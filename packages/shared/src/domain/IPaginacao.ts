export interface IPaginacao {
  size: number;
  page: number
}