export interface IOrdenacao {
  [key: string]: 'asc' | 'desc';
}