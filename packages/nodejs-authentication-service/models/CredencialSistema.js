const mongoose = require('mongoose')
const Credencial = require('./Credencial')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const CredencialSistemaSchema = new mongoose.Schema({
    descricao: {
        type: String,
        required: true,
    },
    token: {
        type: String,
        required: true,
        index: {
            unique: true,
        }
    }
}, opts)

module.exports = Credencial.discriminator("CredencialSistema", CredencialSistemaSchema, 'sistema');