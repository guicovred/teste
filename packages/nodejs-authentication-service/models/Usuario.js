const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },
    timestamps: {}
};

const UsuarioSchema = new mongoose.Schema({
    nome: {
        type: String
    },
    emailPrincipal: {
        type: String
    },
    telefonePrincipal: {
        type: String
    },
    avatar: {
        type: String
    },
    bloqueado: {
        type: Boolean,
        default: false
    },
}, opts)

module.exports = mongoose.model("Usuario", UsuarioSchema);