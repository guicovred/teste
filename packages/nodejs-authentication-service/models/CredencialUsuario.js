const mongoose = require('mongoose')
const Credencial = require('./Credencial')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const CredencialUsuarioSchema = new mongoose.Schema({
    login: {
        type: String,
        required: true,
        index: {
            unique: true,
        }
    },
    usuario: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Usuario',
        required: true,
    },
    password: {
        type: String,
        required: true,
    }
}, opts)

CredencialUsuarioSchema.virtual('ativa').get(function () {
    return !this.bloqueada && !this.protegida
})

module.exports = Credencial.discriminator("CredencialUsuario", CredencialUsuarioSchema, 'usuario');