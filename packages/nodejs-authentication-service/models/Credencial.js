const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },
    discriminatorKey: 'type',
    timestamps: {}
};

const CredencialSchema = new mongoose.Schema({
    bloqueada: {
        type: Boolean,
        default: false
    },
    protegidaAte: {
        type: Date,
    },
    tentativas: {
        type: Number,
        required: true,
        default: 0,
    }
}, opts)

CredencialSchema.virtual('protegida').get(function () {
    return this.protegidaAte && this.protegidaAte > Date.now()
});

CredencialSchema.virtual('ativa').get(function () {
    return !this.bloqueada && !this.protegida
})

module.exports = mongoose.model("Credencial", CredencialSchema);