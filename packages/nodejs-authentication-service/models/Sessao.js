const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const SessaoSchema = new mongoose.Schema({
    expiracao: {
        type: Date
    },
    uuid: {
        type: String,
        required: true
    },
    usuario: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Usuario'
    },
    credencial: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Credencial'
    }
}, opts)

SessaoSchema.virtual('ativa').get(function () {
    return !this.expiracao || this.expiracao > Date.now()
});

module.exports = mongoose.model("Sessao", SessaoSchema);