const CredencialUsuario = require("../../models/CredencialUsuario")
const DbService = require("../../mixins/db.mixin")

const SALT_WORK_FACTOR = 10

module.exports = {
    mixins: [DbService(CredencialUsuario)],
    name: "credenciais-usuario",
    version: 1,
    settings: {
        fields: ["_id", "login", "usuario", "bloqueada", "protegida", "protegidaAte", "ativa", "tentativas", "createdAt", "updatedAt"]
    },

    actions: {
        async findByLogin(ctx) {
            if (!ctx.params.login) {
                throw new MoleculerError("Login não informado", 400)
            }

            return this.adapter.findOne({
                login: ctx.params.login
            })
        }
    },

    methods: {
        async hashPassword(ctx) {
            if (ctx.params.password) {
                let hash = await ctx.call("v1.bcrypt.hash", {
                    texto: ctx.params.password,
                    saltFactor: SALT_WORK_FACTOR
                })

                ctx.params.password = hash
                return ctx
            }
        }
    },

    hooks: {
        before: {
            update: ["hashPassword"],
            create: ["hashPassword"]
        }
    }
}