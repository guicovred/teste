const Usuario = require("../../models/Usuario")
const DbService = require("../../mixins/db.mixin")

module.exports = {
    mixins: [DbService(Usuario)],
    name: "usuarios",
    version: 1,
    settings: {
        fields: ["_id", "nome", "emailPrincipal", "telefonePrincipal", "avatar", "bloqueado"]
    }
}