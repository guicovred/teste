const CredencialSistema = require("../../models/CredencialSistema")
const DbService = require("../../mixins/db.mixin")

module.exports = {
    mixins: [DbService(CredencialSistema)],
    name: "credenciais-sistema",
    version: 1,
    settings: {
        fields: ["_id", "descricao", "token", "bloqueada", "protegida", "protegidaAte", "ativa", "tentativas", "createdAt", "updatedAt"]
    }
}