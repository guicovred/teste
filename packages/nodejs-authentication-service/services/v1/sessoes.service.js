const { MoleculerError } = require("moleculer").Errors

const Sessao = require('../../models/Sessao')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(Sessao)],
    name: "sessoes",
    version: 1,

    settings: {
        fields: ["_id", "credencial", "usuario", "uuid", "expiracao", "ativa"],
        populates: {
            "credencial": "v1.credenciais.get"
        }
    },

    methods: {
        async addUUID(ctx) {
            let uuid = await ctx.call("v1.uuid.uuidv4")
            ctx.params.uuid = uuid

            return ctx
        }
    },

    hooks: {
        before: {
            update: ["addUUID"],
            create: ["addUUID"]
        }
    }
}