const Credencial = require("../../models/Credencial")
const DbService = require("../../mixins/db.mixin")

module.exports = {
    mixins: [DbService(Credencial)],
    name: "credenciais",
    version: 1,
    settings: {
        fields: ["_id", "type", "descricao", "usuario", "login", "key", "bloqueada", "protegida", "protegidaAte", "ativa", "tentativas", "createdAt", "updatedAt"],
        populates: {
            "usuario": "v1.usuarios.get"
        }
    }
}