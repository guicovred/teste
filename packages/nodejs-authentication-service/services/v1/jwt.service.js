const jwt = require("jsonwebtoken")
const { MoleculerError } = require("moleculer").Errors
const { promisify } = require("util");

module.exports = {
    name: "jwt",
    version: 1,
    methods: {
        encode: promisify(jwt.sign),
        verify: promisify(jwt.verify),
    },

    settings: {
        JWT_SECRET: process.env.JWT_SECRET || 'WbpV3gqLe9NOyFn7W1y0PmahPXFksDicgWsB9Ur45e17DTqQhy7wYHIELZ000La',
    },

    actions: {
        async encode(ctx) {
            let secret = this.settings.JWT_SECRET

            if (!ctx.params.payload) {
                throw new MoleculerError("Payload não informado", 400)
            }
            if (ctx.params.secret) {
                secret = ctx.params.secret
            }

            let encoded = await this.encode(ctx.params.payload, secret)

            return encoded

        },
        async decode(ctx) {
            let secret = this.settings.JWT_SECRET

            if (!ctx.params.token) {
                throw new MoleculerError("Token não informado", 400)
            }

            if (ctx.params.secret) {
                secret = ctx.params.secret
            }

            let decoded = await this.verify(ctx.params.token, secret)

            return decoded
        }
    }
}