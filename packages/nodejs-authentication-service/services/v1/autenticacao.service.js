const { MoleculerError } = require("moleculer").Errors

module.exports = {
    name: "autenticacao",
    version: 1,

    actions: {
        async resumeSession(ctx) {

            if (!ctx.meta.user || !ctx.meta.sessao || !ctx.meta.credencial) {
                throw new MoleculerError("Não autenticado", 400)
            }

            let sessao = await ctx.call("v1.sessoes.update", {
                id: ctx.meta.sessao
            })

            let token = await ctx.call("v1.jwt.encode", {
                payload: {
                    id: sessao._id,
                    uuid: sessao.uuid
                }
            })

            ctx.meta.$responseHeaders = {
                "Set-Cookie": `USER_SESSION=${token}; ${sessao.expiracao ? `Expires=${sessao.expiracao.toUTCString()};` : ``} path=/; HttpOnly`
            }
        },

        async getUsuario(ctx) {

            if (!ctx.meta.user) {
                throw new MoleculerError("Não autenticado", 400)
            }

            let usuario = await ctx.call("v1.usuarios.get", {
                id: ctx.meta.user
            })

            return usuario
        },

        async updateUsuario(ctx) {

            if (!ctx.meta.sessao) {
                throw new MoleculerError("Não autenticado", 400)
            }

            let id = ctx.meta.user

            let usuario = await ctx.call('v1.usuarios.update', {
                id, ...ctx.params
            })

            return usuario
        },

        async login(ctx) {

            if (!ctx.params.login) {
                throw new MoleculerError("Login não informado", 400)
            }

            if (!ctx.params.password) {
                throw new MoleculerError("Senha não informada", 400)
            }

            let credencial = await ctx.call("v1.credenciais-usuario.findByLogin", {
                login: ctx.params.login
            })

            if (!credencial) {
                throw new MoleculerError("Login ou senha inválidos", 400)
            }

            try {
                if (!credencial.ativa) {
                    throw new MoleculerError("Usuário temporariamente bloqueado por muitas tentativas de login", 400, "TOO_MANY_ATTEMPTS", { bloqueadoAte: credencial.bloqueadoAte })
                }

                let compare = await ctx.call("v1.bcrypt.compare", {
                    hash: credencial.password,
                    texto: ctx.params.password
                })

                if (!compare) {
                    throw new MoleculerError("Login ou senha inválidos", 400)
                }

            } catch (erro) {
                let now = new Date();
                let tentativas = credencial.tentativas;
                let bloqueadoAte = 0

                bloqueadoAte = now.setMinutes(now.getMinutes() + Math.floor(tentativas / 5) * 5)

                await ctx.call("v1.credenciais.update", {
                    id: credencial._id,
                    tentativas: tentativas + 1,
                    bloqueadoAte: bloqueadoAte
                })

                throw erro;
            }

            let usuario = await ctx.call("v1.usuarios.get", {
                id: credencial.usuario.toString(),
                fields: ['bloqueado']
            })

            if (!usuario) {
                throw new MoleculerError("Usuário inexistente", 400)
            }


            if (usuario.bloqueado) {
                throw new MoleculerError("Usuário bloqueado", 400)
            }

            credencial = await ctx.call("v1.credenciais.update", {
                id: credencial._id,
                tentativas: 0,
                bloqueadoAte: 0,
                ultimoLogin: new Date(),
            })

            let expiracao = null
            if (ctx.params.remember) {
                var now = new Date();
                expiracao = now.setDate(now.getDate() + 30);
            }

            let sessao = await ctx.call("v1.sessoes.create", {
                expiracao,
                credencial,
                usuario: credencial.usuario
            })

            let token = await ctx.call("v1.jwt.encode", {
                payload: {
                    id: sessao._id,
                    uuid: sessao.uuid
                }
            })

            ctx.meta.$responseHeaders = {
                "Set-Cookie": `USER_SESSION=${token}; ${sessao.expiracao ? `Expires=${sessao.expiracao.toUTCString()};` : ``} path=/; HttpOnly`
            }

            return true
        },

        async logout(ctx) {
            if (ctx.meta.sessao) {
                await ctx.call("v1.sessoes.remove", {
                    id: ctx.meta.sessao
                })
            }

            ctx.meta.$responseHeaders = {
                "Set-Cookie": `USER_SESSION=; Expires=${new Date().toUTCString()}; path=/; HttpOnly`
            }

            return true
        },

        async refreshToken(ctx) {
            if (ctx.meta.sessao) {
                let now = new Date();
                let expiracao = now.setMinutes(now.getMinutes() + 15);
                let accessToken = await ctx.call("v1.jwt.encode", {
                    payload: {
                        type: "access_token",
                        id: ctx.meta.sessao.credencial,
                        expiracao
                    }
                })

                return accessToken
            }
        }
    }
}