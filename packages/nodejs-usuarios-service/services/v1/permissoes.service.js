const { MoleculerError } = require("moleculer").Errors

const Permissao = require('../../models/Permissao')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(Permissao)],
    name: "permissoes",
    version: 1,

    settings: {
        fields: ["_id", "acoes", "nome", "descricao"],
    }
}