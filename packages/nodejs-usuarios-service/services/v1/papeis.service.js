const { MoleculerError } = require("moleculer").Errors

const Papel = require('../../models/Papel')
const DbService = require('../../mixins/db.mixin')

module.exports = {
    mixins: [DbService(Papel)],
    name: "papeis",
    version: 1,

    settings: {
        fields: ["_id", "nome", "descricao", "permissoes"],
        populates: {
            "permissoes": "v1.permissoes.get"
        }
    },
}