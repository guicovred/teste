const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const PapelSchema = new mongoose.Schema({
    nome: {
        type: String,
        required: true,
        text: true
    },
    descricao: {
        type: String,
        required: true,
        text: true
    },
    permissoes: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Permissao'
        }
    ]
}, opts)

module.exports = mongoose.model("Papel", PapelSchema);