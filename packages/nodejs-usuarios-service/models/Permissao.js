const mongoose = require('mongoose')

const opts = {
    toJSON: {
        virtuals: true
    },

    timestamps: {}
};

const PermissaoSchema = new mongoose.Schema({
    acoes: [
        {
            actionName: {
                type: String,
                required: true
            },
            path: {
                type: String,
                required: true
            }
        }

    ],
    nome: {
        type: String,
        required: true
    },
    descricao: {
        type: String,
        required: true
    }
}, opts)

PermissaoSchema.index({
    nome: 'text',
    descricao: 'text'
});

module.exports = mongoose.model("Permissao", PermissaoSchema);