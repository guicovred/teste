const { MoleculerError } = require("moleculer").Errors
const utm = require('utm')

module.exports = {
    name: "coordenadas",
    version: 1,
    actions: {
        async latLontoUtm(ctx) {

            if (!ctx.params.latitude)
                return Promise.reject(new MoleculerError("Latitude não informada", 500))

            if (!ctx.params.longitude)
                return Promise.reject(new MoleculerError("Longitude não informada", 500))

            return utm.fromLatLon(ctx.params.latitude, ctx.params.longitude)
        }
    }
}